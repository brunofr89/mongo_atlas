variable "microservice" {
  type = string
}

variable "dominio" {
  type = string
}

variable "project_id" {
  type = string
}

variable "database_name" {
  type = string
}

variable "database_version" {
  type = string
}

variable "database_node_type" {
  type = string
}

variable "database_node_type_size" {
  type = string
}

variable "database_storage_size" {
  type = number
}
