output "password_mongo" {
  value = random_password.password.result
  sensitive = true
}