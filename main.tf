locals {
  database_name = var.database_name
  connection_string = replace(
    mongodbatlas_cluster.database.connection_strings[0].private_endpoint[0].connection_string, 
    "mongodb://", 
    "mongodb://${mongodbatlas_database_user.default.username}:${random_password.password.result}@"
  )
}